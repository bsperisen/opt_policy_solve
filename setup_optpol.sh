#!/bin/bash

cd $HOME
mkdir opt

# Install Gurobi
cd opt
wget http://packages.gurobi.com/8.0/gurobi8.0.0_linux64.tar.gz
tar zxf gurobi8.0.0_linux64.tar.gz

# Setup anaconda and the optpol environment.
wget https://repo.anaconda.com/archive/Anaconda2-5.1.0-Linux-x86_64.sh
bash Anaconda2-5.1.0-Linux-x86_64.sh -b
source $HOME/.bashrc
conda create --name optpol -y python=2.7

# Enter environment and start installing dependencies.
source activate optpol
conda install -y -c cvxgrp cvxpy libgcc
conda install -y matplotlib mpi4py

conda config --add channels http://conda.anaconda.org/gurobi
conda install -y gurobi

# Need to install our own version of glibc-2.14 since cypress's version is too old.
wget http://ftp.gnu.org/gnu/glibc/glibc-2.14.tar.gz
tar zxf glibc-2.14.tar.gz
cd glibc-2.14
mkdir build
cd build
../configure --prefix=$HOME/opt/glibc-2.14
make -j4
make install

# Finally, download our code (couldn't get git to work on cypress).
cd $HOME
mkdir opt_policy_solve
cd opt_policy_solve
wget https://gitlab.com/bsperisen/opt_policy_solve/raw/master/opt_policy_solve/gurobi_test.py
wget https://gitlab.com/bsperisen/opt_policy_solve/raw/master/opt_policy_solve/opt_policy_solve.py

echo '
export PATH="$HOME/anaconda2/bin:$PATH"
export LD_LIBRARY_PATH=$HOME/opt/glibc-2.14/lib
export GUROBI_HOME="$HOME/opt/gurobi800/linux64"
export PATH="${PATH}:${GUROBI_HOME}/bin"
export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${GUROBI_HOME}/lib"
' >> .bashrc

