#! usr/bin/python
#
# Written by Benjamin Sperisen to solve for optimal government policies as part
# of an ongoing research project
#
# The code is a fork from Benjamin Tengelsen's Supergametools.

# TODO: errors for inputs for bad Hausdorff inputs
# TODO: errors for inputs for bad cylinder inputs
# TODO: rename b in outerbound

import math
import time
import numpy as np
from scipy.spatial import ConvexHull
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import fractions
import os
import cvxpy

# See if Gurobi is present, setting gurobi_present to indicate.
# We use this when we call the solver (through cvxpy) to use Gurobi if possible, otherwise use ECOS_BB.
import imp
try:
    imp.find_module('gurobipy')
    gurobi_present = True
except:
    gurobi_present = False

__all__ = ['outerbound', 'outerbound_par', 'innerbound', 'innerbound_par', 'hausdorffnorm']     # this list gives what is imported with "from supergametools import *"


def _cylinder(r, n):
    '''
    Returns the unit cylinder that corresponds to the curve r.
    INPUTS:
    r : a vector of radii
    n : number of coordinates to return for each element in r

    OUTPUTS:
    x, y, z: coordinates of points around cylinder
    '''

    # ensure that r is a column vector
    r = np.atleast_2d(r)
    r_rows, r_cols = r.shape

    if r_cols > r_rows:
        r = r.T

    # find points along x and y axes
    points = np.linspace(0, 2*np.pi, n+1)
    x = np.cos(points)*r
    y = np.sin(points)*r

    # find points along z axis
    rpoints = np.atleast_2d(np.linspace(0, 1, len(r)))
    z = np.ones((1, n+1))*rpoints.T

    return x, y, z


def hausdorffnorm(A, B):
    '''
    Finds the hausdorff norm between two matrices A and B.

    INPUTS:
    A: numpy array
    B : numpy array

    OUTPUTS:
    Hausdorff norm between matrices A and B
    '''
    # ensure matrices are 3 dimensional, and shaped conformably
    if len(A.shape) == 1:
        A = np.atleast_2d(A)

    if len(B.shape) == 1:
        B = np.atleast_2d(B)

    A = np.atleast_3d(A)
    B = np.atleast_3d(B)

    x, y, z = B.shape
    A = np.reshape(A, (z, x, y))
    B = np.reshape(B, (z, x, y))

    # find hausdorff norm: starting from A to B
    z, x, y = B.shape
    temp1 = np.tile(np.reshape(B.T, (y, z, x)), (max(A.shape), 1))
    temp2 = np.tile(np.reshape(A.T, (y, x, z)), (1, max(B.shape)))
    D1 = np.min(np.sqrt(np.sum((temp1-temp2)**2, 0)), axis=0)

    # starting from B to A
    temp1 = np.tile(np.reshape(A.T, (y, z, x)), (max(B.shape), 1))
    temp2 = np.tile(np.reshape(B.T, (y, x, z)), (1, max(A.shape)))
    D2 = np.min(np.sqrt(np.sum((temp1-temp2)**2, 0)), axis=0)

    return np.max([D1, D2])


def _loadbalance(n, p):
    '''
    This function assists with load balancing the parallel functions of supergametools.
    It determines how many search gradients are allocated to each process.

    INPUTS:
    n       number of search gradients, int.
    p       number of processes, int.

    OUTPUTS:
    load    list with the number of gradients assigned to each process, list.
    '''
    load = []
    inc = n/p
    R = n - inc*p

    load = [inc for i in range(p)]

    for i in range(int(R)):
        load[i] += 1

    load.append(0)
    load.sort()
    return load


def _perp(a):
    '''
    supports _seg_intersect
    '''
    b = np.empty_like(a)
    b[0] = -a[1]
    b[1] = a[0]
    return b


def _seg_intersect(a1, a2, b1, b2):
    '''
    line segment a given by endpoints a1, a2
    line segment b given by endpoints b1, b2
    '''
    da = a2-a1
    db = b2-b1
    dp = a1-b1
    dap = _perp(da)
    denom = np.dot(dap, db)
    num = float(np.dot(dap, dp))
    return (num / denom)*db + b1


# When indexing "underline" and "bar" values in a single variable, we use the following convention.
# Index s is either 0 (underline, i.e. min payoff) or 1 (bar, i.e. max payoff)
und = 0
bar = 1

# Amount to shift out points when we need collection with dimension for ConvexHull call
eps_dim = 0.001

# Parameter to check for equal vectors
equality_epsilon = 0.1

# Parameter for slight tightening of xi inner problems to get strict inequalities. See draft.
epsilon_star = 0.0001

# Some really big number for the xicheck and xihat problems
infty = 20.

# To be consistent with the "U_mn" formulations instead of "W_mn", we'll use "vcheck" instead of "wcheck" where
# vcheck = (1-delta) * u1(a) + delta * wcheck
class Problem_Collection:
    # In cardinal checking problems, tolerance for primary objective when solving for secondary objective.
    # Note: I think exactly zero should actually be OK, but slight relaxation should be okay if we have to.
    eps_cardinal = 0.001

    def __init__(self, u1, a0, delta, H):
        self.u1 = u1
        A0len, J, A2len = u1.shape
        self.J = J
        self.a0 = a0
        self.delta = delta

        self.H = H
        (L, M) = H.shape

        self.probs = []
        for q in range(L):
            g_q = H[q,:]
            # NW directions
            if g_q[und] <= 0 and g_q[bar] >= 0:
                self.probs.append([J*[None] for i in range(J)])

            elif (g_q[und] > 0 and g_q[bar] >= 0) or (g_q[und] <= 0 and g_q[bar] < 0):
                self.probs.append(J * [None])

            else:
                self.probs.append(None)

        pass

    def solve_MIP(self, objective, constraints):
        prob = cvxpy.Problem(objective, constraints)
        try:
            prob.solve(solver=cvxpy.GUROBI)
            status = prob.status
        except:
            status = 'infeasible'

        return status, prob.value #(status, max_obj_val)

    # Returns (max_objective, Znew_buf), updated if we found a better objective than what was passed (otherwise left unmodified)
    # Note: secondary_objective is only used for cardinal directions, see wrapper method below
    # solve_secondary is boolean set to True only if we are solving for the secondary_objective
    def find_fencepost_candidate(self, W_und, W_bar, W_constraints, a0, g_q, q, max_objective):
        # Abbreviating for convenience TODO clean this up
        delta = self.delta
        del1 = 1 - delta
        J = self.J
        u1 = self.u1

        Znew_buf = None # We will be returning this as the fencepost. If we couldn't find anything feasible, this will be None.

        # Boolean set to true if we're in a cardinal direction (so we have to do secondary solving).
        cardinal_direction = abs(g_q[und]) < 0.0001 or abs(g_q[bar]) < 0.0001

        # Create inner problem variables
        wcheck = cvxpy.Variable(J, J)
        what = cvxpy.Variable(J, J)

        # Build constraints to keep w vars inside W intervals
        wcheck_in_W_constraints = []
        what_in_W_constraints = []
        for m in range(J):
            for n in range(J):
                wcheck_in_W_constraints += [W_und[m, n] <= wcheck[m, n], wcheck[m, n] <= W_bar[m, n]]
                what_in_W_constraints += [W_und[m, n] <= what[m, n], what[m, n] <= W_bar[m, n]]

        # Add incentive compatibility constraints
        wcheck_IC_constraints = []
        what_IC_constraints = []
        for i in range(J):
            wcheck_IC_constraints.append([])
            what_IC_constraints.append([])
            for m in range(J):
                if m != i:
                    wcheck_IC_constraints[i].append(del1 * u1[a0,i,i] + delta * wcheck[i,i] >= del1 * u1[a0,m,i] + delta * wcheck[m,i])
                    what_IC_constraints[i].append(del1 * u1[a0, i, i] + delta * what[i, i] >= del1 * u1[a0, m, i] + delta * what[m, i])

        # Abbreviations for "U inner values"
        Ucheck = []
        Uhat = []
        Uund = []
        Ubar = []
        for i in range(J):
            Ucheck.append([])
            Uhat.append([])
            Uund.append([])
            Ubar.append([])
            for j in range(J):
                Ucheck[i].append(del1 * u1[a0,i,j] + delta * wcheck[i,j])
                Uhat[i].append(del1 * u1[a0,i,j] + delta * what[i,j])
                Uund[i].append(del1 * u1[a0,i,j] + delta * W_und[i,j])
                Ubar[i].append(del1 * u1[a0,i,j] + delta * W_bar[i,j])

        # Building inner check constraints for directions with E or S component
        bins = [cvxpy.Bool(J) for i in range(J)]
        b_mute = cvxpy.Bool(J)
        x_maxmin = cvxpy.Variable(J)
        c_check = cvxpy.Variable(1)
        c_hat = cvxpy.Variable(1)

        inner_op_constraints = []
        for i in range(J):
            inner_op_constraints += [cvxpy.sum_entries(bins[i]) >= 1]
            for k in range(J):
                inner_op_constraints += [x_maxmin[i] <= del1*u1[a0,k,i] + delta*W_und[k,i] + infty * (1-bins[i][k])]
                inner_op_constraints += [Uund[k][i] <= x_maxmin[i]]
            inner_op_constraints += [epsilon_star * b_mute[i] - infty * (1-b_mute[i])
                                      <= x_maxmin[i] - (del1*u1[a0,i,i] + delta * W_bar[i,i])]
            inner_op_constraints += [infty * b_mute[i] >= x_maxmin[i] - (del1*u1[a0,i,i] + delta * W_bar[i,i])]
        inner_op_constraints += [cvxpy.sum_entries(b_mute) <= J-1]

        ccheck_constraints = []
        chat_constraints = []
        for i in range(J):
            ccheck_constraints += [c_check <= x_maxmin[i] + infty * b_mute[i]]
            chat_constraints += [c_hat >= (del1 * u1[a0,i,i] + delta * W_bar[i,i]) - infty * b_mute[i]]

        check_max_constraints = inner_op_constraints + ccheck_constraints
        hat_min_constraints = inner_op_constraints + chat_constraints

        # Handle NW directions
        if g_q[und] <= 0 and g_q[bar] >= 0:
            for i in range(self.J):
                for j in range(self.J):
                    if self.probs[q][i][j] is None:
                        constraints = W_constraints + wcheck_in_W_constraints + what_in_W_constraints\
                                      + wcheck_IC_constraints[i] + what_IC_constraints[j]

                        # Set objective to search direction.
                        objective = cvxpy.Maximize(g_q[und] * Ucheck[i][i] + g_q[bar] * Uhat[j][j])

                        status, cand_obj_val = self.solve_MIP(objective, constraints) #, max_objective)

                        if status == 'optimal' and cand_obj_val > max_objective:
                            max_objective = cand_obj_val
                            Znew_buf = np.array([Ucheck[i][i].value, Uhat[j][j].value])

            # If this is also a cardinal direction, we want to make sure the unweighted direction is correctly solved.
            if cardinal_direction:
                secondary_objective = -float('inf')
                for i in range(self.J):
                    for j in range(self.J):
                        if self.probs[q][i][j] is None:
                            # Start with same constraints from before
                            ordinary_constraints = W_constraints + wcheck_in_W_constraints + what_in_W_constraints\
                                                   + wcheck_IC_constraints[i] + what_IC_constraints[j]
                            # Turn primary objective into constraint
                            primary_val_constraint = [g_q[und] * Ucheck[i][i] + g_q[bar] * Uhat[j][j]
                                                      >= max_objective - Problem_Collection.eps_cardinal]

                            # Put secondary objective as full objective
                            if g_q[und] == 0.:
                                secondary_objective_cvxpy = cvxpy.Maximize(-Ucheck[i][i])
                            else:
                                secondary_objective_cvxpy = cvxpy.Maximize(Uhat[j][j])

                            status, cand_sec_obj_val = self.solve_MIP(secondary_objective_cvxpy, ordinary_constraints + primary_val_constraint)

                            if status == 'optimal':
                                if cand_sec_obj_val > secondary_objective:
                                    secondary_objective = cand_sec_obj_val
                                    Znew_buf = np.array([Ucheck[i][i].value, Uhat[j][j].value])

        # NE directions
        elif g_q[und] > 0 and g_q[bar] >= 0:
            for j in range(self.J):
                if self.probs[q][j] is None:
                    objective = cvxpy.Maximize(g_q[und] * c_check + g_q[bar] * (del1 * u1[a0,j,j] + delta * what[j,j]))

                    constraints = W_constraints + check_max_constraints + what_in_W_constraints + what_IC_constraints[j]

                    status, cand_obj_val = self.solve_MIP(objective, constraints)
                    if status == 'optimal':
                        if cand_obj_val > max_objective:
                            max_objective = cand_obj_val
                            Znew_buf = np.array([c_check.value, del1 * u1[a0,j,j] + delta * what.value[j,j]])

            if cardinal_direction:
                secondary_objective = -float('inf')
                for j in range(self.J):
                    ordinary_constraints = W_constraints + check_max_constraints + what_in_W_constraints + what_IC_constraints[j]

                    # Turn primary objective into constraint
                    primary_val_constraint = [g_q[und] * c_check + g_q[bar] * (del1 * u1[a0,j,j] + delta * what[j,j])
                                              >= max_objective - Problem_Collection.eps_cardinal]

                    secondary_objective_cvxpy = cvxpy.Maximize(Uhat[j][j])
                    status, cand_obj_val = self.solve_MIP( secondary_objective_cvxpy, ordinary_constraints + primary_val_constraint)

                    if status == 'optimal' and cand_obj_val > secondary_objective:
                        secondary_objective = cand_obj_val
                        Znew_buf = np.array([c_check.value, Uhat[j][j].value])
                        # TODO Fix hack
                        if c_check.value - (del1 * u1[a0,j,j] + delta * what.value[j,j]) < 0.01:
                            Znew_buf = np.array([c_check.value, Uhat[j][j].value])


        # SW directions
        elif g_q[und] <= 0 and g_q[bar] < 0:
            for i in range(J):
                objective = cvxpy.Maximize(g_q[und] * (del1 * u1[a0,i,i] + delta * wcheck[i,i]) + g_q[bar] * c_hat)
                constraints = W_constraints +  wcheck_in_W_constraints + wcheck_IC_constraints[i] + hat_min_constraints

                status, cand_obj_val = self.solve_MIP(objective, constraints)
                if status == 'optimal':
                    if cand_obj_val > max_objective:
                        max_objective = cand_obj_val
                        Znew_buf = np.array([Ucheck[i][i].value, c_hat.value])


            if cardinal_direction:
                secondary_objective = -float('inf')
                for i in range(J):
                    # Turn primary objective into constraint
                    ordinary_constraints = W_constraints +  wcheck_in_W_constraints + wcheck_IC_constraints[i] + hat_min_constraints
                    primary_val_constraint = [g_q[und] * (del1 * u1[a0,i,i] + delta * wcheck[i,i]) + g_q[bar] * c_hat
                                              >= max_objective - Problem_Collection.eps_cardinal]

                    secondary_objective_cvxpy = cvxpy.Maximize(-Ucheck[i][i])
                    status, cand_obj_val = self.solve_MIP(secondary_objective_cvxpy, ordinary_constraints + primary_val_constraint)

                    if status == 'optimal':
                        if cand_obj_val > secondary_objective:
                            secondary_objective = cand_obj_val
                            Znew_buf = np.array([del1 * u1[a0,i,i] + delta * wcheck.value[i,i], c_hat.value])

        # SE directions
        else:
            objective = cvxpy.Maximize(g_q[und] * c_check + g_q[bar] * c_hat)
            constraints = W_constraints +  inner_op_constraints + ccheck_constraints + chat_constraints

            status, cand_obj_val = self.solve_MIP(objective, constraints)

            if status == 'optimal':
                if cand_obj_val > max_objective:
                    max_objective = cand_obj_val
                    Znew_buf = np.array([c_check.value, c_hat.value])

        if (Znew_buf is not None) and (Znew_buf[1] < -0.01):
            print 'uh oh, problematic point?', Znew_buf
        return (Znew_buf, max_objective)

def outerbound_par(u1, n_grad=8, delta=0.8, tol=1e-4, MaxIter=200, plot=True, display=True, Hausdorff=False):
    '''
    This method computes the outerbound approximation (quite a few things from old JYC stuff are probably left here).
    INPUTS:
    u1:         matrix with stage payoffs, indexed a0, m, n (TODO Clarify this)
    p1:         payoff matrix for player 1. numpy array(n,n, ndim=2)
    n_grad:     number of search gradients. int.
    delta:      discount factor. float.
    plot:       True will generate plots. boolean.
    tol:        Minimum tolerable convergence error. float.
    MaxIter:    Maximum number of iterations allowed. int.
    display:    Option to display output during iterations. boolean.
    Hausdorff:  Option to measure error using Hausdorff norm instead of standard relative error measure. boolean.

    OUTPUT:
    Z           Vertices for set of supergame equilibria. array.
    figure      Plot of conhull and vertices from earlier iterations
                (True by default. Set Plot==False to turn off)
    '''
    # MPI preliminaries
    from mpi4py import MPI
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    size = comm.Get_size()
    print 'This is process rank=', rank, 'size=', size

    #-------------------------------------------------------------------------------
    # start timer
    #-------------------------------------------------------------------------------
    if rank == 0:
        start_time = time.time()

    #-------------------------------------------------------------------------------
    # check inputs for correctness
    #-------------------------------------------------------------------------------
    # Monitoring structure, for now with perfect monitoring
    A0len,J,A2len = u1.shape
    if J != A2len:
        raise Exception("payoff matrices must be square") # and of the same size")

    if n_grad < 4:
        raise Exception("insufficient number of search gradients")

    #-------------------------------------------------------------------------------
    # parameters
    #-------------------------------------------------------------------------------
    del1 = 1-delta

    #-------------------------------------------------------------------------------
    # gradients and tangency points
    #-------------------------------------------------------------------------------
    # For now, require n_grad to be a multiple of 4 because we want to include the cardinal directions.
    if n_grad % 4 != 0:
        raise Exception("number of search gradients should be a multiple of 4 to include cardinal directions")
    incr = 360.0/n_grad
    cum = -90 # Made this 0 again # 45 # Used to be 0, experimenting to see if strong symmetry can be accurately solved for with different directions
    start_cum = cum
    H = []
    Z = []
    while cum < 360 + start_cum:
        x = np.sin(cum * np.pi/180)
        y = np.cos(cum * np.pi/180)
        H.append((x, y))

        # Start with feasible collection F
        # We add epsilon dimension to placate ConvexHull call below
        # This creates a circle to the northwest of F, with radius eps_dim.
        # TODO is there a better way to do this?
        Z.append((np.min(u1) - 2*eps_dim + x*eps_dim, np.max(u1) + 2*eps_dim + y*eps_dim))
        cum = cum + incr

    # Make sure cardinal directions are *exactly* 0 and 1 (used later for NW). TODO Check if there's a way to avoid this.
    H[0] = [-1., 0.]
    H[n_grad/4] = [-0.,1.]
    H[n_grad/2] = [1.,0.]
    H[3*n_grad/4] = [-0,-1.]


    # C = np.atleast_2d(np.sum(np.array(Z) * np.array(H), axis=1))
    Z = np.array(Z)
    L = len(H)
    H = np.array(H) # Need this to be np array for later indexing syntax.

    # plot initial feasible collection
    if rank == 0 and plot is True:
        plt.figure()
        plt.plot(Z[:, und], Z[:, bar], 'rx')
        plt.xlim([np.min(u1)-1,np.max(u1)+1])
        plt.ylim([np.min(u1)-1,np.max(u1)+1])

    #-------------------------------------------------------------------------------
    # Begin optimization portion of program
    #-------------------------------------------------------------------------------
    #-------------------------------------------------------------------------------
    # iterative parameteres
    #-------------------------------------------------------------------------------

    iter = 0
    tolZ = 1
    Zold = np.zeros((len(Z), 2))
    slices = _loadbalance(n_grad, size)
    cumslices = np.cumsum(np.array(slices))
    slice = slices[rank+1]

    if rank == 0 and display is True:
        print('Outer Hyperplane Approximation')

    # Construct inner constraints
    inner_constraints = [Problem_Collection(u1, a0, delta, H) for a0 in range(A0len)]

    NW_dir_indices = [p for p in range(n_grad) if H[p,und] <= 0. and H[p,bar] >= 0.]
    # NW_dirs = np.array([H[p,:] for p in NW_dir_indices])

    #-------------------------------------------------------------------------------
    # Begin algorithm for outer hyperplane approximation
    #-------------------------------------------------------------------------------
    # if rank <= n:
    while tolZ > tol and iter < MaxIter:

        # Construct iteration
        WlaCla_Buffer = np.zeros((L, 3))
        WlaCla_entry = np.zeros((L, 3))

        # loop through L search gradients
        for s in range(slice):
            q = int(cumslices[rank]) + s

            fourth = n_grad / 4
            quad_ind = int(q / fourth)
            quads = ['NW','NE','SE','SW']
            print 'doing quad', quads[quad_ind]

            # Note: x (decision variable of linear program) is v = (1-del)*u(a) + del*w, i.e. it is the averaged (generated) payoff,
            # as opposed to w (as written in JYC).
            max_objective = -float('inf')
            Znew_buf = np.zeros(2) - infty

            g_q = H[q, :]

            # Boolean set to True for directions unweighted in one dimension. TODO explain one component is zero
            cardinal_direction = abs(g_q[und]) < 0.0001 or abs(g_q[bar]) < 0.0001

            # Define interval variables
            W_und = cvxpy.Variable(J,J)
            W_bar = cvxpy.Variable(J,J)

            # Construct Wbf constraints
            co_Z_ineqs = ConvexHull(Z).equations # Store all inequalities describing co(Z)
            (n_rows,n_cols) = co_Z_ineqs.shape

            # Strip out all the inequalities bounding W from NW directions
            # In other words, the remaining constraints will leave W intervals unbounded in NW directions
            # (but bound them in other directions).
            non_NW_ineqs = np.array([co_Z_ineqs[k, :] for k in range(n_rows)
                                           if not (co_Z_ineqs[k, und] <= 0. and co_Z_ineqs[k, bar] >= 0.)])
            n_ineq_rows, n_ineq_cols = non_NW_ineqs.shape

            # Add the constraints for every W_und.
            Wbf_constraints_cvxpy = []
            for m in range(J):
                for n in range(J):
                    # Add NW constraints
                    for p in range(len(NW_dir_indices)):
                        # Note this depends on all the NW directions being at the top (i.e. first rows) of Z.
                        Wbf_constraints_cvxpy.append(H[p,und] * W_und[m,n] + H[p,bar] * W_bar[m,n] <= np.dot(H[p,:], Z[p,:]))

                    # For each row in the matrix non_NW_ineqs, append a row to the Wbf_constraints, adding a cvxpy constraint
                    for row in range(n_ineq_rows):
                        Wbf_constraints_cvxpy.append(non_NW_ineqs[row,0]*W_und[m,n] + non_NW_ineqs[row, 1]*W_bar[m,n]
                                                     <= -non_NW_ineqs[row,2])

            # Add symmetry constraints
            W_symmetry_constraints = []
            for m in range(J):
                for n in range(m+1):
                    W_symmetry_constraints.append(W_und[m,n] == W_und[n,m])
                    W_symmetry_constraints.append(W_bar[m,n] == W_bar[n,m])

            # All constraints on W:
            W_constraints = Wbf_constraints_cvxpy + W_symmetry_constraints

            for a0 in range(A0len):
                # Solve for fencepost candidate.
                Znew_cand, cand_obj_val = inner_constraints[a0].find_fencepost_candidate(W_und, W_bar, W_constraints, a0, g_q, q, max_objective)

                if cand_obj_val > max_objective:
                    max_objective = cand_obj_val
                    Znew_buf = Znew_cand

            if max_objective == -float('inf'): # Couldn't find a feasible value
                print "ERROR: nothing feasible, iter", iter, ' direction:', q, H[q,:]
                exit(1)

            WlaCla_entry[q, 0:2] = Znew_buf
            WlaCla_entry[q, 2] = np.dot(Znew_buf, np.array(H[q,:]).T)

        #----------------------------------------------------------------
        # gather all the pieces
        #----------------------------------------------------------------
        comm.Allreduce(WlaCla_entry, WlaCla_Buffer, op=MPI.SUM)
        Wla = WlaCla_Buffer[:, 0:2]
        Cla = WlaCla_Buffer[:, 2].T

        for p in range(L):
            Z[p, :] = Wla[p, :]

        #----------------------------------------------------------------
        # Measure convergence
        #----------------------------------------------------------------
        if Hausdorff is True:
            tolZ = hausdorffnorm(Z, Zold)
        else:
            if rank == 0:
                print 'Zold'
                print Zold
                print 'Z'
                print Z
            max_dist = 0.
            rows,cols = Z.shape
            # For each new fencepost, find the closest old point, and take Manhatten distance.
            for i in range(rows):
                closest_dist = float('inf')
                for j in range(rows):
                    dist = sum(abs(Z[i,:] - Zold[j,:]))
                    if dist < closest_dist:
                        closest_dist = dist
                if closest_dist > max_dist:
                    max_dist = closest_dist
                if rank == 0:
                    print 'dist:', i, closest_dist

            tolZ = max_dist # np.max(np.max(np.abs(Z-Zold)/(1.+abs(Zold)), axis=0))

        if rank == 0:
            print 'At iteration ', iter, '. Z:', Z

        #----------------------------------------------------------------
        # Add points of Z to plot
        #----------------------------------------------------------------
        if rank == 0:
            if iter == MaxIter:
                print('No Convergence in allowed number of iterations \n')
                break

            #----------------------------------------------------------------
            # Print results
            #----------------------------------------------------------------
            if display is True: # and np.mod(iter, 5) == 0:
                print('iteration: %d \t tolerance: %f' % (iter, tolZ))
                # print('diff:', abs(Z-Zold))

            if plot is True:
                plt.plot(Z[:, 0], Z[:, 1], 'o')
                # plt.show()

        #----------------------------------------------------------------
        # update iterative parameters
        #----------------------------------------------------------------
        Zold = Z.copy()
        iter += 1

    if rank == 0:
        #-------------------------------------------------------------------------------
        # Find convex hull of most recent Z array and plot
        #-------------------------------------------------------------------------------
        # TODO Check if this is actually convex hull -- it shouldn't be, right?
        # TODO Also need to change to outer fencepost bound
        H_perp = np.hstack((-np.atleast_2d(H[:, 1]).T, np.atleast_2d(H[:, 0]).T)) * 2

        lines = []
        for p in range(L):
            lines.append((Z[p, :] + H_perp[p, :], Z[p, :] - H_perp[p, :]))

        lines.append(lines[0])
        vertices = []
        for p in range(1, L+1):
            ai, a2 = lines[p-1]
            b1, b2 = lines[p]
            vertices.append(_seg_intersect(ai, a2, b1, b2))

        vertices = np.array(vertices)

        if plot is True:
            Vplot = np.vstack((vertices, vertices[0, :]))
            plt.plot(Vplot[:, 0], Vplot[:, 1], 'r-')
            plt.xlabel('W_und')
            plt.ylabel('W_bar')
            plt.grid()

        #-------------------------------------------------------------------------------
        # Plot final results and display
        #-------------------------------------------------------------------------------
        if iter < MaxIter and display is True:
            print('Convergence after %d iterations' % (iter))

            # evaluate and display elapsed time
            elapsed_time = time.time() - start_time
            print('Elapsed time is %f seconds' % (elapsed_time))

        #display plot
        if plot is True:
            plt.show()

        return vertices



# Stage game:
# -------------          -------------
# | 3,3 | 0,4 |          | 2,2 |-1,3 |
# | 4,0 | 1,1 |          | 3,-1| 0,0 |
# -------------          -------------
#       R                      A

if __name__ == '__main__':
    
    # Plain old prisoner's dilemma (no government)
    # u1 = np.zeros((1,2,2))
    # u1[0,:,:] = np.array([[3., 0.], [4., 1.]])

    # Prisoner's dilemma with government R and A actions.
    u1 = np.zeros((2, 2, 2))
    u1[0,:,:] = np.array([[3., 0.], [4., 1.]]) # R
    u1[1,:,:] = np.array([[2., -1.], [3., 0.]]) # A

    # 2nd price auctions example
    # max_bid = 2
    # bid_increm = 1.
    # n_bids = int(1 + max_bid / bid_increm)
    # bid_grid = np.array([i * bid_increm for i in range(n_bids)])
    # u1 = np.zeros((2, n_bids, n_bids))
    # for theta in range(1, 3, 1):
    #     g_index = theta - 1
    #     for p1_ind in range(n_bids):
    #         for p2_ind in range(n_bids):
    #             if p1_ind < p2_ind:
    #                 u1[g_index,p1_ind,p2_ind] = 0.
    #             elif p1_ind == p2_ind:
    #                 u1[g_index,p1_ind,p2_ind] = 0.5 * (theta - bid_grid[p1_ind])
    #             else: # p1 > p2
    #                 u1[g_index,p1_ind,p2_ind] = theta - bid_grid[p1_ind]

    print 'u1[0,:,:]=', u1[0,:,:]
    print 'u1[1,:,:]=', u1[1,:,:]
    Z_outer = outerbound_par(u1, delta=0.4, n_grad=16, Hausdorff=False, tol=0.01, MaxIter=200)
    print 'Final outer bound: ', Z_outer
