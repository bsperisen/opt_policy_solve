======================================================================
Optimal Policy under Collusion Solver
======================================================================
This program is Python code to solve for optimal government policies under collusion as part of an ongoing research project. 
 - Author: Benjamin Sperisen <bsperise@tulane.edu>
 - This is a modification of the code from Benjamin Tengelsen's Supergametools project, available at <https://github.com/btengels/supergametools>.

======================================================================
Dependencies
======================================================================
The code calls the following python libraries:

	- numpy -- <http://www.numpy.org>
	- scipy -- <http://www.scipy.org>
	- matplotlib -- <http://matplotlib.org>
	- CVXOPT -- <http://cvxopt.org>
	- mpi4py -- <http://mpi4py.scipy.org>
	- GLPK -- <https://www.gnu.org/software/glpk>

The software is only tested for Python 2.7. This has only been tested on Ubuntu 16.04. I don't fully remember what it took for me to configure the dependencies, but I believe there are problems with the default Ubuntu package for mpi4py and must be installed via pip instead of apt-get. CVXOPT must also be compiled with support for GLPK.

======================================================================
To Use
======================================================================
You can run a parallel version with the command:

	mpirun -np [# of processes] python opt_policy_solve.py

======================================================================
Acknowledgements
======================================================================
Thanks to Benjamin Tengelsen for well-written, very clear code that gave this code a great starting point.
